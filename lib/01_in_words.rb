ONES = {
  0 => "zero",
  1 => "one",
  2 => "two",
  3 => "three",
  4 => "four",
  5 => "five",
  6 => "six",
  7 => "seven",
  8 => "eight",
  9 => "nine"
}

TEENS = {
  10 => "ten",
  11 => "eleven",
  12 => "twelve",
  13 => "thirteen",
  14 => "fourteen",
  15 => "fifteen",
  16 => "sixteen",
  17 => "seventeen",
  18 => "eighteen",
  19 => "nineteen"
}

TENS = {
  20 => "twenty",
  30 => "thirty",
  40 => "forty",
  50 => "fifty",
  60 => "sixty",
  70 => "seventy",
  80 => "eighty",
  90 => "ninety"
}

LARGER_NUMS = {
100 => "hundred",
1000 => "thousand",
1_000_000 => "million",
1_000_000_000 => "billion",
1_000_000_000_000 => "trillion"
}

class Integer

  def in_words
    case 
    when self < 10
      return ONES[self]
    when self < 20
      return TEENS[self]
    when self < 100
      self % 10 == 0 ? TENS[self]: TENS[(self / 10)*10] + " " + ONES[self % 10]
    else
      magnitude = find_closest
      magnitude_words = (self/magnitude).in_words + " " + LARGER_NUMS[magnitude]
      if self % magnitude == 0  
        return magnitude_words 
      else
        magnitude_words + " " + (self % magnitude).in_words
      end
    end

  end

  def find_closest
    LARGER_NUMS.keys.select {|key| key <= self}.last
  end
end